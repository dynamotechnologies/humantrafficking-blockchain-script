#!/bin/sh

DATADIR="./data"
GENESIS=./CustomGenesis.json
NETWORKID=22392
IDENTITY="DynamoIDChain"
PORT=30303
RPCPORT=8544
RPCADDR="127.0.0.1"
WSPORT=8546
WSADDR="0.0.0.0"
NOPEERS=3

# Initialize the private blockchain
geth --identity $IDENTITY --datadir $DATADIR  init $GENESIS

#Copy the unlocking account info
cp ./UTC--2018* ./data/keystore/

#Run service
geth  --vmdebug --debug --nat "none" --datadir $DATADIR --identity $IDENTITY  --networkid $NETWORKID --port $PORT --nodiscover --rpc  --rpcport $RPCPORT --rpcaddr $RPCADDR --rpcapi "eth,net,web3,db,personal,admin,debug" --maxpeers $NOPEERS  --rpccorsdomain "*" --unlock 0x92a998e2d04497ad3a453aaf3da9b1e86e04bc67 --password "./password"

